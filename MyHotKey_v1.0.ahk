; 上下左右
!i::
send {up}
return
!k::
send {down}
return
!j::
send {left}
return
!l::
send {right}
return

; 上下左右选
!+i::
send +{up}
return
!+k::
send +{down}
return
!+j::
send +{left}
return
!+l::
send +{right}
return

; HOME
!u::
send {home}
return
; HOME选
!+u::
send +{home}
return

; END
!o::
send {end}
return
; END选
!+o::
send +{end}
return

; 虚拟桌面
#WheelUp::
send #^{left}
return
LAlt & XButton2::
send #^{left}
return
RAlt & -::
send #^{left}
return

#WheelDown::
send #^{right}
return
LAlt & XButton1::
send #^{right}
return
RAlt & =::
send #^{right}
return

; 虚拟回车
#Space::
send {Enter}
return


; Backspace
!h::
send {Backspace}
return
; Delete
!+h::
send {Delete}
return



; PgUp
!,::
send {PgUp}
return
; PgDn
!.::
send {PgDn}
return


; KillShiftSpace
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
<+space::
send ^.
return