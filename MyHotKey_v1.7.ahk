; !==ALT
; +==SHIFT
; ^==CTRL
; #==Win
; ========================= 上下左右 =========================
CapsLock & I::
	if GetKeyState("alt") = 0
        Send {Up}         
    else                   
        Send +{Up}
    return   
CapsLock & K::
	if GetKeyState("alt") = 0
        Send {Down}         
    else                   
        Send +{Down}
    return   
CapsLock & J::
	if GetKeyState("alt") = 0
        Send {Left}
    else                   
        Send +{Left}
    return   
CapsLock & L::
	if GetKeyState("alt") = 0
        Send {Right}         
    else                   
        Send +{Right}
    return


; ========================= 左手左右 =========================
CapsLock & s::
if GetKeyState("alt") = 0                                       
        Send {Left}                                         
    else                                                         
        Send ^s ; C+S
    return   
CapsLock & f:: Send {Right} 
CapsLock & e:: Send {Up}
CapsLock & d:: Send {Down}
CapsLock & w:: Send {WheelUp 8} 
	return
CapsLock & r:: Send {WheelDown 8} 
	return

; ========================= Home & End =========================
CapsLock & U::
	if GetKeyState("alt") = 0
        Send {Home}         
    else                   
        Send +{Home}
    return
CapsLock & O::
	if GetKeyState("alt") = 0
        Send {End}         
    else                   
        Send +{End}
    return
;; ========================= PgUp & PgDn =========================
CapsLock & Y:: Send {PgUp}         
CapsLock & P:: Send {PgDn}         
;; ========================= Backspace & Delete & 标点=========================
CapsLock & ,:: Send {Backspace}         
CapsLock & .:: Send {Delete}   

!,::Send {Text},
!.::Send {Text}.
!+,::Send {Text}<
!+.::Send {Text}>

!;::Send {Text};
!'::Send {Text}'
!+;::Send {Text}:
!+'::Send {Text}"
!+/::Send {Text}?
!+4::Send {Text}$
!+\::Send {Text}|

CapsLock & 9:: Send {Text}(
CapsLock & 0:: Send {Text})
CapsLock & [:: Send {{}
CapsLock & ]:: Send {}}
CapsLock & \:: Send {Text}\

<+1::Send {Text}! ;左Shift+1 == 英文叹号

;; ========================= 按词移动 ===============================
CapsLock & H::
	if GetKeyState("alt") = 0
        Send ^{Left}        
    else                   
        Send ^+{Left}
    return
; 分号不接^+!#时，需要转义
CapsLock & `;::
	if GetKeyState("alt") = 0
        Send ^{Right}        
    else                   
        Send ^+{Right}
    return
;; ========================= 虚拟桌面 =========================
; 上一个桌面
CapsLock & XButton2::Send #^{Left}
CapsLock & -::Send #^{Left}
; 下一个桌面
CapsLock & XButton1::Send #^{Right}
CapsLock & =::Send #^{Right}

; ========================= KillShiftSpace =========================
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
<+Space::Send ^{Space}
^.::Return
;Send ^.
;Return

; ========================= V1.2 多媒体 =========================
; 右侧Ctrl
>^PgUp::Send {Volume_Up}
>^PgDn::Send {Volume_Down}
>^Home::Send {Media_Play_Pause}
>^End::Send {Volume_Mute}
>^Insert::Send {Media_Prev}
>^Delete::Send {Media_Next}

; ========================= V1.4 CapsLock =========================
; SetCapsLockState, AlwaysOff
; CapsLock::Send, {CapsLock}
LShift & CapsLock::
	; T: 获取切换状态(仅对可以切换的按键才有效, 例如 CapsLock, NumLock, ScrollLock 和 Insert)
	; 获取的值为 1(true) 表明按键是 "on"(打开的), 而 0(false) 表明它是 "off"(关闭的).
	if GetKeyState("CapsLock","T") = 1 
        SetCapsLockState AlwaysOff     
	else                                                          
        SetCapsLockState AlwaysOn
	Return  
CapsLock::
	Send {Esc}
	Return
; ========================= Enter =========================
CapsLock & 1:: Send {Enter}		
; ========================= Menu =========================
;CapsLock & 2:: Send {AppsKey}
; ========================= BackSpace =========================
CapsLock & 2:: Send {BackSpace}
; ========================= Delete =========================
CapsLock & 3:: Send {Delete}

; ========================= Ctrl+Alt+Shift+F1 =========================
; CapsLock & Space:: 
; 	Send {Pause}
; 	Return
; ========================= Control =========================
CapsLock & z::
if GetKeyState("alt") = 0                                       
        Send ^z                                           
    else                                                         
        Send ^y
    return           
CapsLock & x:: Send ^x                                           
CapsLock & c:: Send ^c                                           
CapsLock & v:: Send ^v                                           
CapsLock & a:: Send ^a    
CapsLock & b:: Send ^z   ; 相当于 Ctrl+Z


; ========================= 上下5行 =========================
CapsLock & n::
	Loop, 5
	{
   		Send {Up}
	}
	Sleep, 10
	return
CapsLock & m::
	Loop, 5
	{
   		Send {Down}
	}
	Sleep, 10
	return

;; ========================= NumberArea =========================
; TODO 改成Ctrl+CapsLock开关NumbLock
;CapsLock & M:: Send 0		
;CapsLock & J:: Send 1		
;CapsLock & K:: Send 2		
;CapsLock & L:: Send 3		
;CapsLock & U:: Send 4		
;CapsLock & I:: Send 5		
;CapsLock & O:: Send 6		
;CapsLock & 7:: Send 7		
;CapsLock & 8:: Send 8		
;CapsLock & 9:: Send 9		
;; ========================= EnglishPunctuation =========================
;CapsLock & .:: .			
;CapsLock & ,:: ,			


; ========================= 作废代码 =========================
; ========================= 虚拟回车 =========================
;#Space::
;send {Enter}
;return
; ========================= Like WheelScroll =========================
; CapsLock & =:: 
; 	Send {PgDn}
; 	Send {WheelUp 1}
; 	return
; CapsLock & -:: 
; 	Send {PgUp}
; 	Send {WheelDown 1}
; 	return







