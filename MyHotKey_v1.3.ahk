; !==ALT
; +==SHIFT
; ^==CTRL
; =============上下左右=============
!i::send {up}
!k::send {down}
!j::send {left}
!l::send {right}

; =============上下左右选=============
!+i::
send +{up}
return
!+k::
send +{down}
return
!+j::
send +{left}
return
!+l::
send +{right}
return
; =============HOME&END=============
; HOME
!u::
send {home}
return
; HOME选
!+u::
send +{home}
return

; END
!o::
send {end}
return
; END选
!+o::
send +{end}
return

; =============虚拟桌面=============
; #WheelUp::
; send #^{left}
; return
LAlt & XButton2::
send #^{left}
return

RAlt & -::
send #^{left}
return

;#WheelDown::
;send #^{right}
;return
LAlt & XButton1::
send #^{right}
return

RAlt & =::
send #^{right}
return

; =============虚拟回车=============
#Space::
send {Enter}
return


; =============Backspace=============
; !y::	; ALT+<
!,:: ; ALT+<
send {Backspace}
return
; =============Delete=============
; !p::	; ALT+>
!.:: ; ALT+>
send {Delete}
return

; =============按词移动=============
<!h::	; LAlt+H	前移一词
send ^{left}
return
!+h::	; LAlt+LShift+H	前移并选择一词
send ^+{left}
return

<!;::	; LAlt+;	后移一词
send ^{right}
return
!+;::	; LAlt+LShift+；后移并选择一词
send ^+{right}
return



; PgUp
; !,::
!y:: ; ALT+Y
send {PgUp}
return
; PgDn
; !.::
!p:: ; ALT+P
send {PgDn}
return


; KillShiftSpace
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
<+space::
send ^.
return


; =============V1.2 音量控制=============
; Alt+PgUp	音量+
!PgUp::
send {Volume_Up}
return
; Alt+PgDn	音量-
!PgDn::
send {Volume_Down}
return
; Alt+Home	播放/暂停
!Home::
send {Media_Play_Pause}
return
; Alt+End	静音
!End::
send {Volume_Mute}
return