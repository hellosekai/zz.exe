; !==ALT
; +==SHIFT
; ^==CTRL
; =============上下左右=============
!i::
send {up}
return
!k::
send {down}
return
!j::
send {left}
return
!l::
send {right}
return

; =============上下左右选=============
!+i::
send +{up}
return
!+k::
send +{down}
return
!+j::
send +{left}
return
!+l::
send +{right}
return
; =============HOME&END=============
; HOME
!u::
send {home}
return
; HOME选
!+u::
send +{home}
return

; END
!o::
send {end}
return
; END选
!+o::
send +{end}
return

; =============虚拟桌面=============
; #WheelUp::
; send #^{left}
; return
LAlt & XButton2::
send #^{left}
return

RAlt & -::
send #^{left}
return

;#WheelDown::
;send #^{right}
;return
LAlt & XButton1::
send #^{right}
return

RAlt & =::
send #^{right}
return

; =============虚拟回车=============
#Space::
send {Enter}
return


; =============Backspace=============
!y::	; Alt+Y
send {Backspace}
return
; =============Delete=============
!p::	; ALT+P
send {Delete}
return

; =============按词移动=============
<!h::	; LAlt+H	前移一词
send ^{left}
return
!+h::	; LAlt+LShift+H	前移并选择一词
send ^+{left}
return

<!;::	; LAlt+;	后移一词
send ^{right}
return
!+;::	; LAlt+LShift+；后移并选择一词
send ^+{right}
return



; PgUp
!,::
send {PgUp}
return
; PgDn
!.::
send {PgDn}
return


; KillShiftSpace
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
<+space::
send ^.
return