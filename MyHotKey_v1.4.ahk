; !==ALT
; +==SHIFT
; ^==CTRL
; #==Win
; ========================= 上下左右 =========================
!I::Send {Up}
!K::Send {Down}
!J::Send {Left}
!L::Send {Right}
; ========================= 上下左右选 =========================
!+I::Send +{Up}
!+K::Send +{Down}
!+J::Send +{Left}
!+L::Send +{Right}
; ========================= Home & End =========================
!U::Send {Home} ; HOME
!+U::Send +{Home} ; HOME选
!O::Send {End} ; END
!+O::Send +{End} ; END选
; ========================= PgUp & PgDn =========================
!Y::Send {PgUp} ; PgUp
!P::Send {PgDn} ; PgDn
; ========================= Backspace & Delete =========================
!,::Send {Backspace} ; ALT+<
!.::Send {Delete} ; ALT+>
; ========================= 按词移动 ===============================
; 根据文档，分号（;）不需要转义，也不需要换行 => 实测通过
<!h::Send ^{Left} ; LAlt+H	前移一词
!+h::Send ^+{Left} ; LAlt+LShift+H	前移并选择一词
<!;::Send ^{Right} ; LAlt+; 后移一词
!+;::Send ^+{Right} ; LAlt+LShift+；后移并选择一词

; ========================= 虚拟桌面 =========================
; 上一个桌面
LAlt & XButton2::Send #^{Left}
RAlt & -::Send #^{Left}
; 下一个桌面
LAlt & XButton1::Send #^{Right}
RAlt & =::Send #^{Right}

; ========================= KillShiftSpace =========================
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
<+Space::
Send ^.
Return

; ========================= V1.2 音量控制 =========================
!PgUp::Send {Volume_Up} ; Alt+PgUp	音量+
!PgDn::Send {Volume_Down} ; Alt+PgDn	音量-
!Home::Send {Media_Play_Pause} ; Alt+Home	播放/暂停
!End::Send {Volume_Mute} ; Alt+End	静音


; ========================= V1.4 CapsLock =========================
; SetCapsLockState, AlwaysOff
; CapsLock::Send, {CapsLock}
LAlt & CapsLock::
	; T: 获取切换状态(仅对可以切换的按键才有效, 例如 CapsLock, NumLock, ScrollLock 和 Insert)
	; 获取的值为 1(true) 表明按键是 "on"(打开的), 而 0(false) 表明它是 "off"(关闭的).                                   
	if GetKeyState("CapsLock","T") = 1 
        SetCapsLockState AlwaysOff     
	else                                                          
        SetCapsLockState AlwaysOn
	Return  
CapsLock::
	Send, ^{Space}
	Return
; ========================= NumberArea =========================
CapsLock & M:: Send, 0		
CapsLock & J:: Send, 1		
CapsLock & K:: Send, 2		
CapsLock & L:: Send, 3		
CapsLock & U:: Send, 4		
CapsLock & I:: Send, 5		
CapsLock & O:: Send, 6		
CapsLock & 7:: Send, 7		
CapsLock & 8:: Send, 8		
CapsLock & 9:: Send, 9		
; ========================= EnglishPunctuation =========================
CapsLock & .:: .			
CapsLock & ,:: ,			
; ========================= Enter =========================
CapsLock & e:: Send, {Enter}		
; ========================= Menu =========================
CapsLock & r:: Send, {AppsKey}		
; ========================= Control =========================
CapsLock & z::
if GetKeyState("shift") = 0                                       
        Send, ^z                                           
    else                                                          
        Send, ^y
    return           
CapsLock & x:: Send, ^x                                           
CapsLock & c:: Send, ^c                                           
CapsLock & v:: Send, ^v                                           
CapsLock & a:: Send, ^a    
CapsLock & y:: Send, ^y
; ========================= BackSpace =========================
CapsLock & b:: Send, {BackSpace}




; ========================= 作废代码 =========================
; ========================= 虚拟回车 =========================
;#Space::
;send {Enter}
;return
; ========================= Like WheelScroll =========================
; CapsLock & =:: 
; 	Send {PgDn}
; 	Send {WheelUp 1}
; 	return
; CapsLock & -:: 
; 	Send {PgUp}
; 	Send {WheelDown 1}
; 	return







